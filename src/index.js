import React    from 'react';
import ReactDOM from 'react-dom';
import './index.css';

/**
 * Square component function
 * @return {*}
 */
function Square(props) {
    return (
        <button className="square"
                onClick={() => props.onClick()}>
            {props.value}
        </button>
    );
}

/**
 * Composant Board
 */
class Board extends React.Component {

    /**
     * Affiche un Square
     * @return {Square}
     */
    renderSquare(i) {
        return (
            <Square value={this.props.squares[i]}
                    onClick={() => this.props.onClick(i)}/>
        );
    }

    /**
     * Affiche le composant Board
     * @return {*}
     */
    render() {

        // Init du board
        let board = [];
        for (let line = 0; line < 3; line++) {

            // Init de la ligne courante dans le board
            let currentLine = [];
            for (let col = 0; col < 3; col++) {
                const index = line * 3 + col;
                currentLine.push(this.renderSquare(index));
            }

            // On ajoute la ligne au board
            board.push(
                <div className="board-row">{currentLine}</div>
            );
        }

        return (
            <div>{board}</div>
        );
    }
}

/**
 * Composant Game
 */
class Game extends React.Component {

    /**
     * Game constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            history    : [
                {
                    squares  : Array(9).fill(null),
                    lastMove : null,
                },
            ],
            stepNumber : 0,
            xIsNext    : true,
        };
    }

    /**
     * Affiche le composant Game
     * @return {*}
     */
    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner  = calculateWinner(current.squares);

        /*
         * Transforme notre tableau d'historique en tableau d'element React
         * Tableau qui sera directement afficher dans le `return`
         */
        const moves = history.map((step, move) => {

            // Coordonnées du coup
            let lastMoveCoords = '';
            if (step.lastMove !== null) {
                lastMoveCoords = ' (' + step.lastMove[0] + ', ' + step.lastMove[1] + ')';
            }

            // Description dans l'historique
            let desc = move ?
                       'Revenir au tour n°' + move :
                       'Revenir au début de la partie';
            desc     = desc + lastMoveCoords;

            // Coup courant affiché en gras
            if (this.state.stepNumber === move) {
                desc = (<strong>{desc}</strong>);
            }

            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });

        const status = winner !== null
                       ? winner + ' a gagné !'
                       : 'Prochain joueur : ' + (this.state.xIsNext ? 'X' : 'O');

        return (
            <div className="game">
                <div className="game-board">
                    <Board squares={current.squares}
                           onClick={(i) => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }

    /**
     * Gère le clic sur un Square
     * @param {int|void} i
     */
    handleClick(i) {
        /*
         * Créé une copie du tableau history
         * C'est une manière plus optimale de gérer la modification du state
         * car la référence vers this.history change, React n'a donc pas a faire une
         * comparaison profonde des states courant et précédent
         * (en tout cas c'est ce que j'ai compris)
         */
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();

        if (
            calculateWinner(squares) !== null
            || squares[i] !== null
        ) {
            return;
        }

        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history    : history.concat([{
                squares  : squares,
                lastMove : [Math.floor(i / 3), i % 3],
            }]),
            stepNumber : history.length,
            xIsNext    : !this.state.xIsNext,
        });
    }

    /**
     * Déclenche l'affichage de coup sélectionné via l'historique
     * @param stepNumber
     */
    jumpTo(stepNumber) {
        this.setState({
            stepNumber : stepNumber,
            xIsNext    : (stepNumber % 2) === 0,
        });
    }
}

/**
 * Détermine le vainqueur de la partie
 * @param squares
 * @return {null|*}
 */
function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}

// ========================================

ReactDOM.render(
    <Game/>,
    document.getElementById('root')
);
